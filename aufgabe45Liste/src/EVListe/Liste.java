package EVListe;
class Liste
{
   private ListElement kopf;
   private ListElement aktuell;
   private ListElement vorgaenger;


   boolean istLeer()
   {
      return kopf == null;
   }


   void durchlaufe()
   {
      // Die lokale Variable elem verweist auf das gerade betrachtete Listenelement.
      ListElement elem = kopf;
      while (elem != null)
      {
         // bearbeite aktuelles Element, z.B.
         System.out.print(elem.getDaten() + " ");
         // gehe ein Element weiter
         elem = elem.getNaechstes();
      }
      System.out.println();
   }

   int zaehleElemente()
   {
	   if (kopf == null) {return 0;}
	   if (kopf.getNaechstes() == null) return 1;
	   
	   ListElement elem = kopf;
	   int counter=0;
	   do
	   {
		   counter++;
		   elem=elem.getNaechstes();
	   }  while(elem != null);
	   
	   return counter;
   }


   boolean finde(int einObject)
   {
      vorgaenger = null;
      aktuell = kopf;
      while (aktuell != null)
      {
         if (einObject == aktuell.getDaten())
            return true;
         else
         {
            vorgaenger = aktuell;
            aktuell = aktuell.getNaechstes();
         }
      }
      return false;
   }

   // letztes und vorletztes Listenelement ermitteln
   void findeEnde()
   {
	   vorgaenger=null;
	   ListElement elem=kopf;
	
	   while (elem.getNaechstes() != null);
	   {
		   vorgaenger = elem;
		   elem = elem.getNaechstes();
	   }

	   System.out.println(vorgaenger.getDaten() + " " + elem.getDaten());
		   
   }


   int getAktuelleDaten()
   {
      // aktuelles Element muss vorhanden sein
      if (aktuell == null)
         throw new NullPointerException("kein aktuelles Element");

      return aktuell.getDaten();
   }

   int getKopfDaten()
   {
	   return kopf.getDaten();
   }

   int getEndeDaten()
   {
	   ListElement elem=kopf;
	   
	   do
	   {
		   elem = elem.getNaechstes();
	   } while (elem.getNaechstes() != null);
	   
	   return elem.getDaten();
   }


   void einfuegeKopf(int neuesObject)
   {
	   if (kopf==null) {
		   aktuell = kopf;
		   aktuell.setDaten(neuesObject);
	   }
	   else {
		   ListElement elem = new ListElement(neuesObject,kopf);
		   kopf = elem;
	   }
   }

   void einfuegeHinter(int neuesObject)
   {
      // Vorhandenes Element muss angegeben sein
      if (aktuell == null)
    	  throw new NullPointerException("kein aktuelles Element");

      // Einfuegen hinter Element aktuell
      ListElement neu =
         new ListElement(neuesObject, aktuell.getNaechstes());
      aktuell.setNaechstes(neu);
   }

   void einfuegeVor(int neuesObject)
   {
	   if (istLeer() == true) { kopf.setDaten(neuesObject);}
	   
	   if (aktuell == null) { throw new NullPointerException("kein aktuelles Element");}
	   
	   vorgaenger = kopf;
	   
	   while(vorgaenger.getNaechstes() != aktuell)
	   {
		   vorgaenger = vorgaenger.getNaechstes();
	   }
	   ListElement newElem = new ListElement(neuesObject,aktuell);
	   vorgaenger.setNaechstes(newElem);
   }

   void einfuegeEnde(int neuesObject)
   {
	   if (istLeer() == true) { kopf.setDaten(neuesObject);}
	   
	   aktuell = kopf;
	   while(aktuell.getNaechstes() != null) {
		   aktuell = aktuell.getNaechstes();
	   }
	   aktuell.setNaechstes(new ListElement(neuesObject));
   }


   void loescheNachfolger()
   {
	   if (aktuell == null) throw new NullPointerException("kein aktuelles Element");
	   ListElement nachfolger = aktuell.getNaechstes();
	   
	   if (nachfolger.getNaechstes() == null) { aktuell.setNaechstes(null); }
	   else if (nachfolger.getNaechstes() != null) {
		   aktuell.setNaechstes(nachfolger.getNaechstes());
	   }
   }

   void loescheElement()
   {
	   if (aktuell == null) throw new NullPointerException("kein aktuelles Element");
	   
	   vorgaenger = kopf;
	   while (vorgaenger.getNaechstes() != aktuell) {
		   vorgaenger = vorgaenger.getNaechstes();
	   }
	   vorgaenger.setNaechstes(aktuell.getNaechstes());
   }

   void loescheKopf()
   {
	   if (kopf == null) throw new NullPointerException("kein aktuelles Element");
	   if (kopf.getNaechstes() == null) throw new NullPointerException("kein aktuelles Element");
	   kopf = kopf.getNaechstes();
   }

   void loescheEnde()
   {
	   if (istLeer() == true || kopf.getNaechstes() == null) throw new NullPointerException("kein aktuelles Element");
	   aktuell = kopf;
	   while (aktuell.getNaechstes() != null) {
		   vorgaenger = aktuell;
		   aktuell = aktuell.getNaechstes();
	   }
	   vorgaenger.setNaechstes(null);
   }

}
