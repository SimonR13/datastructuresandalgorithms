public class InsertionSort{
  private static long vergleiche;
  private static long vertauschungen;

public static void insertionsort (int list[])
{
        int j;
        int hilf;
        
    for (int i=1; i<list.length; i++)
      {
         hilf = list[i];
         
         // finde den Einfuegeplatz j des Elements hilf
         j = i-1;
         
         while ((j >= 0) && (list[j] > hilf))
          {
            j=j-1;
            vergleiche++;
          }
         if ((j >= 0) && (list[j] <= hilf)) vergleiche++;
         j=j+1;
       
         // Verschieben der Elemente des Zielteils zwischen j und i nach rechts
         for (int k=i; k>j; k--)
          {
                  list[k]=list[k-1];
                  vertauschungen++;
                }
                      
         // fuege hilf ein  
         list[j]=hilf;
         
         //Ausgabe des Feldes nach i-tem Lauf
         /*
         System.out.print("Feld nach Durchlauf "+ i + " :");
         for (int z=0; z<list.length; z++)
          System.out.print( list[z]+", " );
         System.out.println();
         */
      }
}
         

        public static void main(String args[])
        {       vergleiche = 0;
          vertauschungen = 0;
                //int a[] = {12,4,3,11};
                int a[] = {56,22,79,27,9,30,61,4,69,38,52,89,23,17,68,30};
                //int a[] = {3,5};
                //int a[] = {5,3};
                //int a[] = {3};
                insertionsort(a);
                
                for (int i=0; i<a.length; i++)
                        System.out.print( a[i]+", " );
                System.out.println();
                System.out.println("Anzahl Vergleiche: " + vergleiche );
    System.out.println("Anzahl Vertauschungen: " + vertauschungen);
    
    vergleiche = 0;
    vertauschungen = 0;
    int[] zahlenfolge = new int[100000];            
        for (int i=1; i< 100000; i++)                   
        zahlenfolge[i] = (int) (Math.random() * 100000);  
                                         
        MyTimer timer1 = new MyTimer();                   
        insertionsort(zahlenfolge);
        System.out.println("Verbrauchte Zeit: " + timer1.getElapsed());
          System.out.println("Es wurden "+vergleiche+" Vergleiche benoetigt");
          System.out.println("Es wurden "+vertauschungen+" Vertauschungen benoetigt");
          
        }
}
  