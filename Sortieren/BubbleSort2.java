  
  public class BubbleSort2 {                      // Klasse BubbleSort
  
  
  private static long vergleiche;
  private static long vertauschungen;
  

  
 public static void sort(int[] a) {           
    int tmp; 
    
    for(int i=a.length-1; i>0; i--)
  {
    for(int j=0; j < i; j++)
    {
    if (a[j] > a[j+1])
      {
        tmp = a[j];                    
                a[j] = a[j+1];                 
                a[j+1] = tmp; 
                vertauschungen++;
            }
          vergleiche++;
        }
          /*
          for (int k=0; k<a.length; k++)
                        System.out.print( a[k]+" " );
                System.out.println(); 
                */
                // System.out.println("Anz. Vergleiche" + vergleiche);
     }
    }

  
  
  
  public static void main(String args[])
        { 
          vergleiche = 0;
                // int a[] = {9,1,2,3,4,5,6,7,8};
                int a[] = {56,22,79,27,9,30,61,4,69,38,52,89,23,17,68,30};
                sort(a);
                
                for (int i=0; i<a.length; i++)
                        System.out.print( a[i]+", " );
                System.out.println();
               
                
    System.out.println("Anzahl Vergleiche: " + vergleiche );
    System.out.println("Anzahl Vertauschungen: " + vertauschungen );
    
    
    vergleiche = vertauschungen = 0;
    int[] zahlenfolge = new int[100000];            
        for (int i=1; i< 100000; i++)                   
        zahlenfolge[i] = (int) (Math.random() * 100000);  
                                         
        MyTimer timer1 = new MyTimer();                   
        sort(zahlenfolge);
        System.out.println("Verbrauchte Zeit: " + timer1.getElapsed());
          System.out.println("Es wurden "+vergleiche+" Vergleiche benoetigt");
          System.out.println("Anzahl Vertauschungen: " + vertauschungen );
        }
} 
