

/***************************  HeapSort.java  **********************************/

/** Iteratives Sortieren mit Heapsort 
 *  Entnimm einem Heap so lange das kleinste Element, bis er leer ist.
 *  Die entnommenen Elemente werden im selben Array gespeichert.
 */

import java.util.*;

public class Heap {
  
  private static long vergleiche;

  private static void lasse_Einsinken (int[] feld, int root, int m) 
  {   
    int child;
    int temp;
    temp = feld[root];
    child = 2* root +1;
    // vergleiche++;
    while ( child <= m)
    {
    if ((child<m)&&(feld[child]>feld[child+1]))
      {
        child++;
        vergleiche++;
      }
      else //notwendig f�r das Zaehlen von Vergleichen
      {
        if (child<m)
          vergleiche++;
      }
      if (temp <= feld[child])
      {
        vergleiche++;
        break;    //Position gefunden
      }
      else
      {
        feld[(child - 1) / 2] = feld[child];  // * Sohn nach oben
        child = 2*child +1;
        vergleiche++;
      }
    }
      feld[(child-1)/2] = temp; //Positionieren der Wurzel
  }

  public static void heapsort (int feld[] ) 
  {          // statische Methode heapsort 
    // Heap aufbauen
    int i, j;
    int temp;
    /* 1.Schritt: Heap erzeugen aus Eingabefeld */
    int n = feld.length - 1;
    for (i=(n-1)/2; i>=0; i--)
      lasse_Einsinken(feld, i, n);
    
    /*2.Schritt: Schleife �ber i; Schreibe das i-kleinste Element
      nach feld[n-i+1]; lasse Wurzel einsinken */
    for (i=n; i>=0; i--)
    { temp=feld[i];
      feld[i] = feld[0];
      feld[0] = temp;
      lasse_Einsinken(feld, 0, i-1);
    }
   }
   
  public static void main(String args[])
  { 
        vergleiche = 0;
         int a[] = {56,22,79,27,9,30,61,4,69,38,52,89,23,17,68,30};
         heapsort(a);
                
         for (int i=0; i<a.length; i++)
                System.out.print( a[i]+"  " );
                System.out.println();
    System.out.println("Anzahl Vergleiche: " + vergleiche );
    
    int[] zahlenfolge = new int[100000];
    for (int i=1; i< 100000; i++)
          zahlenfolge[i] = (int) (Math.random() * 100000);
              
      Date timer1 = new Date();
      long vorher = timer1.getTime();
      System.out.println("vorher: " + vorher);
      vergleiche = 0;
      // Laufzeit bestimmen 
      heapsort(zahlenfolge);
      Date timer2 = new Date();
      long nachher = timer2.getTime();
      System.out.println("nachher: " + nachher);
      long diff = nachher - vorher;
      System.out.println("verbrauchte CPU: " + diff);
      System.out.println("Anzahl Vergleiche: " + vergleiche );

    }
} 

