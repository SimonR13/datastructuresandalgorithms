package intset2;
public class Intset2
{
	static final int N = 10;
	
	// Array feur die eigentlichen Daten
	int daten[];
	
	// aktuelle Anzahl gueltiger Daten im Array
	int anzahlElemente;
		
	public static Intset2 empty()
	{
		Intset2 menge = new Intset2();
		menge.daten = new int[N];
		menge.anzahlElemente = 0;
		return menge;
	}
		
	public static Intset2 insert(Intset2 menge, int element)
	{
		if (isEmpty(menge))
		{
			menge.daten[0] = element;
			menge.anzahlElemente++;
		}
		else
		{
			int einfuegepos = binsuch(menge, element, 0, menge.anzahlElemente-1);
			if (menge.daten[einfuegepos] == element) //Element bereits enthalten
				return menge;
			else
			{
				if (menge.anzahlElemente == menge.daten.length)
					System.out.println("Feld ist voll !!!");
				else
				{
					if (menge.daten[einfuegepos] < element)
						einfuegepos++;
					shiftright(menge, einfuegepos);
					menge.daten[einfuegepos] = element;
					menge.anzahlElemente++;
				}
			}
		}
		return menge;
	}
	
	public static Intset2 delete(Intset2 menge, int element)
	{
		if (isEmpty(menge))
			return menge;
		else
		{
			int loeschpos = binsuch(menge, element, 0, menge.anzahlElemente-1);
			if (menge.daten[loeschpos] != element)
				return menge;
			else
			{
				shiftleft(menge, loeschpos);
				menge.anzahlElemente--;
				return menge;
			}
		}
	}
	
	public static boolean contains(Intset2 menge, int element)
	{
		int position = binsuch(menge, element, 0, menge.anzahlElemente-1);
		return menge.daten[position] == element;
	}
	
	public static boolean isEmpty(Intset2 menge)
	{
		return menge.anzahlElemente == 0;
	}
	
	private static void shiftleft(Intset2 menge, int position)
	{
		for (int i=position; i<menge.anzahlElemente-1; i++)
			menge.daten[i] = menge.daten[i+1];
	}
	
	private static void shiftright(Intset2 menge, int position)
	{
		for (int i=menge.anzahlElemente; i>position; i--)
			menge.daten[i] = menge.daten[i-1];
	}
	
	private static int binsuch(Intset2 menge, int element, int low, int high)
	{
		int mitte = low + (high-low)/2;
		if (menge.daten[mitte] != element && low != high)
		{
			if (menge.daten[mitte] < element)
				return binsuch(menge, element, mitte+1, high);
			else
				return binsuch(menge, element, low, mitte-1);
		}
		else
			return mitte;
	}
	
	public static void ausgabe(Intset2 menge)
	{
		for (int i=0; i<menge.anzahlElemente; i++)
		{
			System.out.print(menge.daten[i]);
		};
		System.out.println();
	}
	
	public static void main(String[] args)
	{
		Intset2 menge;
		boolean leereMenge;
		
		menge = empty();
		menge = insert(menge, 1);
		menge = insert(menge, 3);
		menge = insert(menge, 5);
		menge = insert(menge, 4);
		menge = insert(menge, 4);
		menge = insert(menge, 2);
		menge = delete(menge, 4);
		menge = delete(menge, 6);
		ausgabe(menge);
		menge = delete(menge, 1);
		ausgabe(menge);
		leereMenge = isEmpty(menge);
		System.out.println(leereMenge);
	}
}	