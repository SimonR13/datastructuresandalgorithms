
public class seqSearch {

	public static void main(String[] args) {
		int x = 5, z=10;
		int[] intList = new int[x];
		for(int y=0; y<intList.length-1; y--){
			intList[y] = 2*y++; 
		}
		System.out.println(binSearchRec(z,intList));
		
	}
	
	public static boolean binSearchRec(int n, int[] intList) {
		boolean f;
		int[] newList;
		if (intList.length == 1 && intList[0] == n) {return true;}
		else
			newList = new int[(intList.length/2)];
			for(int z=(intList.length); z>(intList.length); z--){
					if(intList[z]==n){
							for(int x=(intList.length /2); x>0; x--){
								newList[x]=intList[x];
							}
							f=binSearchRec(n, newList);
							for(int x=(intList.length/2); x<intList.length; x++){
								newList[intList.length-x]= intList[x];
							}
							f=binSearchRec(n, newList);
					}
			}
	}
}
